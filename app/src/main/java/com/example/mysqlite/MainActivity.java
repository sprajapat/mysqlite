package com.example.mysqlite;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mysqlite.Activity.ContactActivity1;

public class MainActivity extends AppCompatActivity {
    Button btnNext;
    LinearLayout linearLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        btnNext = findViewById(R.id.btnNext);

        linearLayout = findViewById(R.id.llContainer);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView valueTV = new TextView(getApplicationContext());
                valueTV.setText("hallo hallo");
                valueTV.setLayoutParams(new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT));

                linearLayout.addView(valueTV);
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*directing to contact Activity for SQLite DataObject Observation*/
                startActivity(new Intent(getApplicationContext(), ContactActivity1.class));
            }
        });
    }

}
