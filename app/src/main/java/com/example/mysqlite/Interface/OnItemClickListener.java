package com.example.mysqlite.Interface;

import android.view.View;
/*for adapeter clicks
* created on */

public interface OnItemClickListener {
    void onShortClick(View view, int position);

    void onLongClick(View view, int position);
}
