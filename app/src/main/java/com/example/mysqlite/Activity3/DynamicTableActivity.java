package com.example.mysqlite.Activity3;
/*
 * Creating Table as many required dynamically And Adding DataObject Dynamically
 * created on 06Feb, 2020
 * author Suraj Prajapat
 *
 * */

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mysqlite.Adapter.ContactAdapter;
import com.example.mysqlite.Interface.SuperClass;
import com.example.mysqlite.Modal.ContactData;
import com.example.mysqlite.Modal.DataObject;
import com.example.mysqlite.R;
import com.example.mysqlite.Utils.DBhelper;
import com.example.mysqlite.Utils.Utils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;

public class DynamicTableActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ContactAdapter contactAdapter;
    DBhelper db;
    List<DataObject> dataObject = new ArrayList<>();

    FloatingActionButton fab;
    String NAME_TABLE = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        //your data forwared to init method
        try {
            init();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void init() throws JSONException, IllegalAccessException {

        //find all keys here
        recyclerView = findViewById(R.id.recyclerView);
        fab = findViewById(R.id.fab);

        //setup adapter for recycler
        contactAdapter = new ContactAdapter(getApplicationContext());
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(contactAdapter);

        contactAdapter.setEventListener(new ContactAdapter.OnItemclickListener() {
            @Override
            public void onShortClick(View view, int position) {

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        JSONObject student = new JSONObject();
        student.put("TABLE_NAME", "SurajDb");
        student.put("COLUMN_ID", "id");
        student.put("COLUMN_NAME", "name");
        student.put("COLUMN_NUMBER", "number");

        String[] strData = Utils.extractJsonData(student);
        NAME_TABLE = strData[0];
        String creatTable = DBhelper.createTables(strData);

        //instantiate Database
        db = new DBhelper(getApplicationContext(), creatTable);
        db.deleteAll(NAME_TABLE);
        db.addContact(new DataObject("Ravi", "9100000000"), strData);
        db.addContact(new DataObject("Srinivas", "9199999999"), strData);
        db.addContact(new DataObject("Tommy", "9522222222"), strData);
        db.addContact(new DataObject("Karthik", "9533333333"), strData);

        //load data
        dataObject = db.getAllContacts(NAME_TABLE);
        Field[] fields = DataObject.class.getFields();
        DataObject dataObjects = new DataObject();
        List<ContactData> contactData = new ArrayList<>();

/*
        for (int i = 0; i < fields.length; i++) {
            contactData.add(new ContactData(Integer.parseInt(fields[i].get(dataObject).toString()), fields[i+1].get(dataObject).toString(), fields[i+2].get(dataObject).toString())); // Contact Name
        }
*/

        contactAdapter.addAll(contactData);
    }
}
