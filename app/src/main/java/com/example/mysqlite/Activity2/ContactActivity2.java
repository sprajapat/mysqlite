package com.example.mysqlite.Activity2;

import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mysqlite.Adapter.ContactAdapter;
import com.example.mysqlite.Interface.OnItemClickListener;
import com.example.mysqlite.Modal.ContactData;
import com.example.mysqlite.R;
import com.example.mysqlite.Utils.DatabaseHelper;
import com.example.mysqlite.Utils.Utils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ContactActivity2 extends AppCompatActivity {
    RecyclerView recyclerView;
    ContactAdapter contactAdapter;
    DatabaseHelper db;
    List<ContactData> contactData = new ArrayList<>();
    EditText edtNumber, edtName;
    TextView btnAdd;
    FloatingActionButton fab;
    int select;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        //your data forwared to init method
        init();
    }

    private void init() {
        //find all keys here
        recyclerView = findViewById(R.id.recyclerView);
        fab = findViewById(R.id.fab);

        //setup adapter for recycler
        contactAdapter = new ContactAdapter(getApplicationContext());
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(contactAdapter);
        new OnItemClickListener() {
            @Override
            public void onShortClick(View view, int position) {

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        };
        // clicks througth interface from Adapter class
        contactAdapter.setEventListener(new ContactAdapter.OnItemclickListener() {
            @Override
            public void onShortClick(View view, int position) {
                select = 1;
                showDialog(view, position);
            }

            @Override
            public void onLongClick(View view, int position) {
                //delet recycler item as well as database item
                contactAdapter.deleteContacts(position);
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                select = 0;
                showDialog(v, 5);
            }
        });

        //instantiate Database
        db = new DatabaseHelper(getApplicationContext());
        contactData = db.getAllContacts();
        sort();
        contactAdapter.addAll(contactData);
    }

    private void sort() {
        Collections.sort(contactData, new Comparator<ContactData>() {
            @Override
            public int compare(ContactData o1, ContactData o2) {
                return o1.getName().toString().compareTo(o2.getName().toString());
            }
        });
    }

    private void createContact(ContactData contact) {
        long id = db.addContact(contact);
        Utils.showToast(getApplicationContext(), "number Added Succesfully");
        ContactData n = db.getContact(id);
        contactData.add(n);
        int pos = contactAdapter.getItemCount();
        if (n != null) {
            contactAdapter.add(n, pos);
        }
    }

    private void updateContact(ContactData contact, int position) {
//        not updating in current basis implementation remaining
        ContactData n = contactData.get(position);
        n.setName(contact.getName().toString());
        n.setNumber(contact.getNumber().toString());
        db.updateContact(n);
        // refreshing the list
        contactData.set(position, n);
        sort();
//        contactAdapter.notifyItemChanged(position);
        contactAdapter.notifyDataSetChanged();
    }

    public void showDialog(View v, final int pos) {
        View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.popup_add_contact, null);
        final PopupWindow popupWindow = new PopupWindow(view, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        popupWindow.setContentView(view);
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        popupWindow.showAtLocation(v, Gravity.CENTER, 0, 0);
        edtNumber = view.findViewById(R.id.edtNumber);
        edtName = view.findViewById(R.id.edtName);
        btnAdd = view.findViewById(R.id.btnAdd);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edtName.getText().toString())) {
                    Utils.showToast(getApplicationContext(), "Enter Name");
                    return;
                } else if (TextUtils.isEmpty(edtNumber.getText().toString())) {
                    Utils.showToast(getApplicationContext(), "Enter Number");
                    return;
                }
                if (select == 1) {
                    updateContact(new ContactData(edtName.getText().toString(), edtNumber.getText().toString()), pos);
                } else if (select == 0) {
                    createContact(new ContactData(edtName.getText().toString(), edtNumber.getText().toString()));

                }
                popupWindow.dismiss();
            }
        });
    }
}
