package com.example.mysqlite.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mysqlite.Activity3.DynamicTableActivity;
import com.example.mysqlite.Adapter.ContactAdapter;
import com.example.mysqlite.Modal.ContactData;
import com.example.mysqlite.R;
import com.example.mysqlite.Utils.DatabaseHelper;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class ContactActivity1 extends AppCompatActivity {
    RecyclerView recyclerView;
    ContactAdapter contactAdapter;
    DatabaseHelper db;
    List<ContactData> contactData = new ArrayList<>();
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        //your data forwared to init method
        init();
    }

    private void init() {

        //find all keys here
        recyclerView = findViewById(R.id.recyclerView);
        fab = findViewById(R.id.fab);

        //setup adapter for recycler
        contactAdapter = new ContactAdapter(getApplicationContext());
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(contactAdapter);
        //to move next activity
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Contact Activity 2
                startActivity(new Intent(getApplicationContext(), DynamicTableActivity.class));
            }
        });

        //instantiate Database
        db = new DatabaseHelper(getApplicationContext());
        db.deleteAll();//delet previous data from database

        db.addContact1(new ContactData("Ravi", "9100000000"));
        db.addContact1(new ContactData("Srinivas", "9199999999"));
        db.addContact1(new ContactData("Tommy", "9522222222"));
        db.addContact1(new ContactData("Karthik", "9533333333"));
        contactData.clear();
        contactAdapter.addAll(db.getAllContacts1());
    }
}
