package com.example.mysqlite.Utils;
/*
 * Creating Table And Adding DataObject Dynamically
 * created on 06Feb, 2020
 * author Suraj Prajapat
 *
 * */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.ContactsContract;
import android.util.Log;

import com.example.mysqlite.Modal.ContactData;
import com.example.mysqlite.Modal.DataObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class DBhelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Suraj_db";
    private static String TABLE_NAME = "Sur";
    public static String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_NUMBER = "number";
    String CREATE_TABLE;

    public DBhelper(Context context, String CREATE_TABLE) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.CREATE_TABLE = CREATE_TABLE;
    }

    //Creating a String to Create a Table for SQLite
    public static String createTables(String[] values) {
        String CREATE_TABLE2 = "CREATE TABLE " + values[0] + "("
                + values[1] + " INTEGER PRIMARY KEY AUTOINCREMENT";
        for (int i = 2; i < values.length; i++) {
            CREATE_TABLE2 = CREATE_TABLE2 + " ," + values[i] + " TEXT";
        }
        CREATE_TABLE2 = CREATE_TABLE2 + ")";

        return CREATE_TABLE2;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    //done
    public void addContact(DataObject dataObject, String[] strData) throws IllegalAccessException {
        if (strData.length > 1) {
            TABLE_NAME = strData[0];
            COLUMN_ID = strData[1];
        } else {
            return;
        }
        Field[] fields = DataObject.class.getFields();

        DataObject dataObject1 = dataObject;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        for (int i = 2; i < strData.length; i++) {
            values.put(strData[i], fields[i - 1].get(dataObject).toString()); // Contact Name
        }

        db.insert(TABLE_NAME, null, values);
        db.close(); // Closing database connection
    }

    // code to get all contacts in a list view
    public List<DataObject> getAllContacts(String TABLE_NAME) {
        List<DataObject> dataObjects = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                dataObjects.add(new DataObject(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2)));
            } while (cursor.moveToNext());
        }
        return dataObjects;
    }

    public void deleteAll(String TABLE_NAME) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_NAME);
        db.close();
    }
}
/*
 * accessing field of a class then manke them public and access througth the field[]
 *Field[] fields = ContactData.class.getFields();
 *Object value = fields[i].get(contactData)
 *
 * */

/*
 * ContentValues values = new ContentValues();
 *values.put(strData[i], fields[i - 2].get(contactData).toString()); // Contact items
 *
 * */
