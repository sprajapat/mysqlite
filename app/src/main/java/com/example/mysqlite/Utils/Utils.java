package com.example.mysqlite.Utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.Iterator;

public class Utils {
    //Show Toast
    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    //extracting json object into string only values
    public static String[] extractJsonData(JSONObject jsonObject) {
        int length = jsonObject.length();

        Iterator<String> keys = jsonObject.keys();

        String[] value = new String[length];
        String[] values = new String[length];
        int i = 0;
        while (keys.hasNext()) {
            value[i] = keys.next();
            values[i] = jsonObject.optString(value[i]);
            i += 1;
        }
        return values;
    }

    //Creating a String to Create a Table for SQLite
    public static String createTables(String[] values) {
        String CREATE_TABLE2 = "CREATE TABLE " + values[0] + "("
                + values[1] + " INTEGER PRIMARY KEY AUTOINCREMENT";

        for (int i = 2; i < values.length; i++) {
            CREATE_TABLE2 = CREATE_TABLE2 + " ," + values[i] + " TEXT";
        }
        CREATE_TABLE2 = CREATE_TABLE2 + ")";

        Log.e("string > >", CREATE_TABLE2);
        return CREATE_TABLE2;
    }
}
