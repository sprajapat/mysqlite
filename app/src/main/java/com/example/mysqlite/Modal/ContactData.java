package com.example.mysqlite.Modal;

import com.example.mysqlite.Interface.Pointer;
import com.example.mysqlite.Interface.SuperClass;

public class ContactData extends SuperClass {
    public Object name;
    public Object number;
    private Object id;

    public ContactData() {
    }

    public ContactData(String name, String number) {
        this.name = name;
        this.number = number;
    }

    public ContactData(int id, String name, String number) {
        super();
        this.id = id;
        this.name = name;
        this.number = number;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Object getName() {
        return name;
    }

    public Object getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

}
