package com.example.mysqlite.Modal;

import com.example.mysqlite.Interface.Pointer;
import com.example.mysqlite.Interface.SuperClass;

public class DataObject extends SuperClass {
/*    protected T t;

    public void setObj(T t) {
        this.t = t;
    }

    public T getObj() {
        return T;
    }*/

    public Object id;
    public Object name;
    public Object number;

    public DataObject() {
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public DataObject(Object id) {
        this.id = id;
    }

    public DataObject(String name, String number) {
        this.name = name;
        this.number = number;
    }

    public DataObject(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public DataObject(int id, String name, String number) {
        this.id = id;
        this.name = name;
        this.number = number;
    }
}
